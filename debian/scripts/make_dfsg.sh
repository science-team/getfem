#/!/bin/bash

if [ $# -lt 1 ]
then
  echo Please, add the tarball-name
  exit 1
fi

rm -rf ./tmp/
mkdir tmp
tar -xzvf $1 -C ./tmp/
find ./tmp -name \*.so | xargs rm

file="$1"
file_base=${file%.orig.tar.*}

file_new=$file_base+dfsg1.orig.tar.xz

cd tmp; tar -cf - ./* | xz -9 -c - > ../$file_new
