Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: GETFEM
Source: https://getfem.org/
Files-Excluded:
  interface/src/scilab
  compile
  configure
  depcomp
  ltmain.sh
  missing
  py-compile
  test-driver

Files: *
Copyright: 1995-2022, Yves Renard <Yves.Renard@insa-lyon.fr>
           2000-2015, Julien Pommier <Julien.Pommier@insa-toulouse.fr>
           2001-2015, Jeremie Lasry
           2002-2009, Vanessa Lleras
           2002-2008, Michel Salaün
           2002-2008, Houari Khenous
           2002-2008, Jean-Yves Heddebaut
           Thorsten Ottosen
           2009-2022, Konstantinos Poulios <poulios.konstantinos@gmail.com>
           2002-2015, Michel Fournié
License: LGPL-3+
  GetFEM  is  free software;  you  can  redistribute  it  and/or modify it
  under  the  terms  of the  GNU  Lesser General Public License as published
  by  the  Free Software Foundation;  either version 3 of the License,  or
  (at your option) any later version along with the GCC Runtime Library
  Exception either version 3.1 or (at your option) any later version.
  This program  is  distributed  in  the  hope  that it will be useful,  but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or  FITNESS  FOR  A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License and GCC Runtime Library Exception for more details.
  You  should  have received a copy of the GNU Lesser General Public License
  along  with  this program;  if not, write to the Free Software Foundation,
  Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
 .
  On Debian systems the full text of the GNU Lesser General Public
  License can be found in the `/usr/share/common-licenses/LGPL-3'
  file.

Files: superlu/*
Copyright: 1987-1990, John Gilbert
           1992-1994, Univ. of Tennessee
           1992-1994, NAG Ltd.
           1992-1994, Courant Institute
           1992-1994, Argonne National Lab
           1992-1994, Rice University
           1994, Xerox Corporation
           1992-2003, Univ. of California Berkeley
           1997-2003, Xerox Palo Alto Research Center
           1997-2003, Lawrence Berkeley National Lab
           1998-2003, University of Florida
License: BSD-3-clause and BSD/superlu and Xerox
 THIS MATERIAL IS PROVIDED AS IS, WITH ABSOLUTELY NO WARRANTY
 EXPRESSED OR IMPLIED.  ANY USE IS AT YOUR OWN RISK.
 Permission is hereby granted to use or copy this program for any
 purpose, provided the above notices are retained on all copies.
 Permission to modify the code and to distribute modified code is
 granted, provided the above notices are retained, and a notice that
 the code was modified is included with the above copyright notice.
 .
 ==========================================
 BSD/superlu
 The Regents of the University of California, through Lawrence Berkeley National
 Laboratory (subject to receipt of any required approvals from U.S. Dept. of
 Energy)
 .
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 (1) Redistributions of source code must retain the above copyright notice,
 this list of conditions and the following disclaimer.
 (2) Redistributions in binary form must reproduce the above copyright notice,
 this list of conditions and the following disclaimer in the documentation
 and/or other materials provided with the distribution.
 (3) Neither the name of Lawrence Berkeley National Laboratory, U.S. Dept. of
 Energy nor the names of its contributors may be used to endorse or promote
 products derived from this software without specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
 IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 .
 ==========================================
 Xerox
 Xerox Corporation. All rights reserved.
 .
 THIS MATERIAL IS PROVIDED AS IS, WITH ABSOLUTELY NO WARRANTY
 EXPRESSED OR IMPLIED.  ANY USE IS AT YOUR OWN RISK.
 .
 Permission is hereby granted to use or copy this program for any
 purpose, provided the above notices are retained on all copies.
 Permission to modify the code and to distribute modified code is
 granted, provided the above notices are retained, and a notice that
 the code was modified is included with the above copyright notice.

Files: interface/src/matlab/*
       interface/tests/python/*
       bin/*
       contrib/static_contact_gears/static_contact_planetary.py
Copyright: 2006-2012, Yves Renard <Yves.Renard@insa-lyon.fr>
           2006-2012, Julien Pommier <Julien.Pommier@insa-toulouse.fr>
License: LGPL-2+
  GetFEM  is  free software;  you  can  redistribute  it  and/or modify it
  under  the  terms  of the  GNU  Lesser General Public License as published
  by  the  Free Software Foundation;  either version 3 of the License,  or
  (at your option) any later version along with the GCC Runtime Library
  Exception either version 2.1 or (at your option) any later version.
  This program  is  distributed  in  the  hope  that it will be useful,  but
  WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
  or  FITNESS  FOR  A PARTICULAR PURPOSE.  See the GNU Lesser General Public
  License and GCC Runtime Library Exception for more details.
  You  should  have received a copy of the GNU Lesser General Public License
  along  with  this program;  if not, write to the Free Software Foundation,
  Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
 .
  On Debian systems the full text of the GNU Lesser General Public
  License can be found in the `/usr/share/common-licenses/LGPL-2.1'
  file.
